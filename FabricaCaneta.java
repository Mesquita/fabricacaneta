public class FabricaCaneta{
	public static void main(String arg[]){
		//Declaração das variáveis.
		Caneta umaCaneta;
		umaCaneta = new Caneta();
		
		//Inserção dos valores para os métodos sets.
		umaCaneta.setCor("Vermelho");
		umaCaneta.setMaterial("Plástico");
		umaCaneta.setTampa(true);
		
		//Impressão das características da caneta usando os métodos gets.		
		System.out.println("Uma caneta com: ");
		System.out.println("Cor: "+umaCaneta.getCor());
		System.out.println("Material: "+umaCaneta.getMaterial());
		if(umaCaneta.tampa==false){
			System.out.println("Tampa: Sem tampa");
		}
		else{
			System.out.println("Tampa: Com tampa");
		}
		System.out.println("Foi criada com sucesso!");		
	}
}
