class Caneta{
	//Declaração de variáveis.
	private String cor;
	private String material;
	public boolean tampa;
	//Descrição dos métodos de Caneta. Sets e Gets.
	public void setCor(String umaCor){
		cor=umaCor;
	}
	public String getCor(){
		return cor;
	}
	public void setMaterial(String umMaterial){
		material=umMaterial;
	}
	public String getMaterial(){
		return material;
	} 
	public void setTampa(boolean umaTampa){
		tampa=umaTampa;
	}
	public boolean getTampa(){
		return tampa;
	}
 }
